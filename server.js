'use strict';

const express = require('express');
const superAgent = require('superagent'); // Will be used as a proxy server
const cors = require('cors');
const bp = require('body-parser');

// ENV values
const PORT = process.env.PORT || 3000;
const KEY =  process.env.KEY;

// Setting the API domain url.
const __API__ = 'https://exam.net-inspect.com'; 

// Launch the express server and mount cors to it. cors will allow all requests.
const app = express();
app.use(cors());

// ------ Endpoints ----------

// Get data, 10 per page, and uses the offset to change which 10 are being searched.
app.get('/API/v1/qpl/:offset/:pagesize', (req, res) =>{
  console.log('hit GET /qpl')
  console.log(req.params)
  if(!req.params.offset) req.params.offset = 0;
  let offSet = req.params.offset;
  if(!req.params.pagesize) req.params.pagesize = 10;
  let pageSize = req.params.pagesize;

  superAgent.get(`${__API__}/qpl?offset=${offSet}&pageSize=${pageSize}`)
    .set('Authorization', `Bearer ${KEY}`)
    .then(results => res.send(results.text))
    .catch(err => console.log(err))
})

// Test route to check for connection.
app.get('/ping', (req, res) =>{
  res.send('pong')
});

app.listen(PORT, () => console.log(`Listening on port ${PORT}`));