'use strict';

var app = app || {};

(function(module){
  // Is passed an object, typically from the server.
  function Part(rawDataObj){
    Object.keys(rawDataObj).map(key => {
      this[key] = rawDataObj[key]

      // Formatting Values into humand readable text.
      if(key === 'lastUpdatedDateUtc'){
        let date = this[key].split('T')[0].split('-').reverse()
        this[key] = `${date[1]}/${date[0]}/${date[2]}` // Set date as MM/DD/YYYY
        return
      } 
      if(this[key] === true) this[key] = 'Yes'
      if(this[key] === false) this[key] = 'No'
    })
    return this
  }

  Part.$tbodyEl = $('#qpl-body') // Attaching it here so each object doesn't have to reference it in renderToTable.
  Part.all = []
  Part.tableKeys = [
    'partNumber', 
    'revision',
    'partName',
    'toolDieSetNumber',
    'isQualified',
    'openPo',
    'jurisdiction',
    'classification',
    'supplierName',
    'supplierCodes',
    'ctq',
    'lastUpdatedBy',
    'lastUpdatedDateUtc'
  ]
  
  Part.prototype.renderToTable = function(){     
    let $trEl = $('<tr></tr>', {id: this.id})// Create table Row
    Part.tableKeys.map(key => $trEl.append( $('<td></td>', {} ).text(this[key]) )) // For each key, make a td cell, and update it's text with the value @ this[key]
    Part.$tbodyEl.append($trEl)
    return this // Adding this return so that it's a bit more functional.
  }

  module.Part = Part
})(app)
  
