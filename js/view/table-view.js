'use strict';

var app = app || {};

(function(module){

  const __API__ = 'http://localhost:3000/API/v1'
  const tableView = {}

  // ---------Init------------
  // Init function so that a future controller may use it to init the view.
  tableView.init = function(fetch){

    tableView.state = {
      offSet: 0,
      pageSize: 10,
      currentPage: 1,
      maxPage: 10,
      search: '',
      currentSearchResults: [],
      error: false,
      calcMaxPage: function(){
        this.maxPage = Math.ceil(100 / this.pageSize) 
      },
    }
    $('#qpl-body').hide(); // Hide the body on Init to allow for better fade-in and out.
    if(fetch) tableView.getParts() // Initial call to get the first set of parts. Optional.
  }
  
  // --------- Methods ------   

  // AJAX call to API to get parts.
  tableView.getParts = function(){   
    $.get(`${__API__}/qpl/${tableView.state.offSet}/${tableView.state.pageSize}`)
      .then(data => JSON.parse(data))
      .then(data => tableView.toHtml(data))
      .catch(err => {
        tableView.state.error = true;
        $('#api-err').slideDown('slow')
        console.log(err)
      })
  }

  // Logic for what and whem to render to the page.
  tableView.toHtml = function(data){
    $('#qpl-body').empty()
    $('.error').hide() // In case err showed up earlier, hide because this was a success.
    this.state.error = false;
    this.state.parts = data.map(part => new app.Part(part).renderToTable())
    $('#qpl-body').toggle('medium')
    this.renderPageNumbers()
    handleSearch()
  }

  tableView.renderPageNumbers = function(){
    $('#max-page').text(`Page: ${this.state.currentPage} / ${this.state.maxPage}`)
    $('#current').text(this.state.currentPage)
  },

  // ------- Event Listeners ---------
  
  // Handles the drop down
  $('#info-bar').on('change', 'select', () =>{
    $('#qpl-body').toggle('fast')
    tableView.state.pageSize = Number($('#page-size').val())
    tableView.state.offSet = 0
    tableView.state.currentPage = 1
    tableView.state.calcMaxPage()
    tableView.getParts()
  })
  
  // Info-bar page nav buttons.
  $('#info-bar').on('click', 'button', (e) =>{
    let state = tableView.state
  
    switch(e.target.id){

    case 'head' :
      if(state.currentPage <= 1) return
      state.offSet = 0
      state.currentPage = 1
      break;

    case 'prev' : 
      if(state.currentPage <= 1) return
      state.offSet -= state.pageSize
      state.currentPage--
      break;

    case 'next' :
      if(state.currentPage < state.maxPage) state.currentPage++
      else return;
      state.offSet += state.pageSize
      break;

    case 'tail' : 
      if(state.currentPage !== state.maxPage) state.currentPage = state.maxPage
      else return;
      state.offSet = 90
      break;
    }
    
    $('#qpl-body').toggle('fast')
    tableView.state = state;
    tableView.getParts()
  })

  // Handles search functionality.
  $('#search').on('keyup','input', handleSearch)
  $('#search').on('submit', handleSearch)

  function handleSearch(e){
    if(e){
      e.preventDefault()
      if(e.originalEvent.key === 'Shift') return // Shift key edge case. I need to learn regex.
    }
  
    let noMatch = true // Will be true if there's ever a match. handles showing some error prompt.
    let term = tableView.state.search = $('#search-box').val()
    if(!term) return;
    tableView.state.currentSearchResults = [];

    // For each 'part', Loop over the arraty of table keys, and for each key, check if the vale at part[key] === term. 
    // If it doesn't then hide the part, else, indicate that there was a match, and show the part.
    tableView.state.parts.map( part => {
      let tableKeys = app.Part.tableKeys
      let termFound = false
      
      for(let key in tableKeys) if(part[tableKeys[key]] === term) termFound = true;
      
      if(!termFound) return $(`#${part.id}`).hide()
      noMatch = false
      $('#no-match-err').fadeOut('fast')
      
      tableView.state.currentSearchResults.push(part)
      $(`#${part.id}`).show()
      
    })
    if(noMatch){
      $('#no-match-err').fadeIn('fast')
      $('tbody > tr').show();
    }
  }
  tableView.init(true) // Simulated call from a router.
  module.tableView = tableView
})(app)